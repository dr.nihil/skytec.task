package ru.skytec.task.observer.event;

import lombok.Getter;

@Getter
public class ChangeGoldEvent extends Event {
    private final String sourceType;
    private final long sourceId;
    private final long clanId;
    private final int oldValue;
    private final int newValue;

    public static class Builder extends Event.Builder<Builder> {
        private String sourceType;
        private long sourceId;
        private long clanId;
        private int oldValue;
        private int newValue;

        public Builder(EventType eventType) {
            super(eventType);
        }

        public Builder sourceType(String sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        public Builder sourceId(long sourceId) {
            this.sourceId = sourceId;
            return this;
        }

        public Builder clanId(long clanId) {
            this.clanId = clanId;
            return this;
        }

        public Builder oldValue(int oldValue) {
            this.oldValue = oldValue;
            return this;
        }

        public Builder newValue(int newValue) {
            this.newValue = newValue;
            return this;
        }

        @Override
        public ChangeGoldEvent build() {
            return new ChangeGoldEvent(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private ChangeGoldEvent(Builder builder) {
        super(builder);
        this.sourceType = builder.sourceType;
        this.sourceId = builder.sourceId;
        this.clanId = builder.clanId;
        this.oldValue = builder.oldValue;
        this.newValue = builder.newValue;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("{")
                .append("\"timestamp\":").append(timestamp).append(",")
                .append("\"eventType\":").append(eventType.name()).append(",")
                .append("\"sourceType\":").append(sourceType).append(",")
                .append("\"sourceId\":").append(sourceId).append(",")
                .append("\"clanId\":").append(clanId).append(",")
                .append("\"oldValue\":").append(oldValue).append(",")
                .append("\"newValue\":").append(newValue)
                .append("}");
        return str.toString();
    }
}
