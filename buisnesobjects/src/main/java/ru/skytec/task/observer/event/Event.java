package ru.skytec.task.observer.event;

import lombok.Getter;

@Getter
public abstract class Event {
    public enum EventType {ADD, SUB}

    final long timestamp;
    final EventType eventType;

    abstract static class Builder<T extends Builder<T>> {
        EventType eventType;
        long timestamp;

        public Builder(EventType eventType) {
            this.eventType = eventType;
        }

        public T timestamp(long timestamp) {
            this.timestamp = timestamp;
            return self();
        }

        abstract Event build();

        protected abstract T self();
    }

    Event(Builder<?> builder) {
        this.eventType = builder.eventType;
        this.timestamp = builder.timestamp;
    }
}
