package ru.skytec.task.objects;

import lombok.Data;

@Data
public class Clan {
    private final long id;
    private final String name;
    private volatile int gold;

    public int getGold() {
        return gold;
    }

    public void setGold(int gold){
        this.gold = gold;
    }
}
