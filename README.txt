SKYTEC.task

Multithread http server for adding gold to clan

Available URIs

Create clan
POST localhost:8080/clan
body
{
    "name": "Clan name"
}

Get clan by id
GET localhost:8080/clan/{id}

Add gold to clan
PUT localhost:8080/clan/{id}?userId={userId}&gold={amountOfGold}

Get events of adding gold
GET localhost:8080/events?max=10&offset=10

Complete task (add 100 gold)
POST localhost:8080/tasks/{id}?clanId={clanId}