package ru.skytec.task.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import ru.skytec.task.exceptions.RequestException;
import ru.skytec.task.observer.impl.ObserverImpl;
import ru.skytec.task.annotations.*;

import java.util.Map;

public class ObserverHttpHandler extends AbstractHttpHandler {
    private final ObserverImpl observerService;

    @AutoConfig
    public ObserverHttpHandler(ObserverImpl observerService) {
        this.observerService = observerService;
    }

    @Override
    protected String handleGetRequest(HttpExchange httpExchange) {
        Map<String, String> params = parseRequestParamsFromQuery(httpExchange.getRequestURI().getQuery());
        int max = 1000;
        int offset = 0;
        if (params.containsKey("max")) {
            max = Integer.parseInt(params.get("max"));
        }
        if (params.containsKey("offset")) {
            offset = Integer.parseInt(params.get("offset"));
        }
        return observerService.getEvents(max, offset).toString();
    }

    @Override
    protected String handlePutRequest(HttpExchange exchange) throws RequestException {
        throw new RequestException("Method " + exchange.getRequestMethod() + "isn't supported");
    }

    @Override
    protected String handlePostRequest(HttpExchange exchange) throws RequestException {
        throw new RequestException("Method " + exchange.getRequestMethod() + "isn't supported");
    }
}
