package ru.skytec.task.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ru.skytec.task.exceptions.RequestException;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractHttpHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String response = "";
        try {
            if ("GET".equals(exchange.getRequestMethod())) {
                response = handleGetRequest(exchange);
            } else if ("POST".equals(exchange.getRequestMethod())) {
                response = handlePostRequest(exchange);
            } else if ("PUT".equals(exchange.getRequestMethod())) {
                response = handlePutRequest(exchange);
            }
            handleResponse(exchange, response, 200);
        } catch (Exception e) {
            handleResponse(exchange, "Incorrect request: " + e.getMessage(), 500);
        }
    }

    protected abstract String handleGetRequest(HttpExchange exchange) throws RequestException;
    protected abstract String handlePutRequest(HttpExchange exchange) throws RequestException;
    protected abstract String handlePostRequest(HttpExchange exchange) throws RequestException;

    protected long getIdFromPath(String path) {
        return Long.parseLong(path.split("/")[2]);
    }

    protected Map<String, String> parseRequestParamsFromQuery(String query) {
        Map<String, String> params = new HashMap<>();
        if (query != null) {
            Arrays.stream(query.split("&"))
                    .forEach(p -> params.put(p.split("=")[0], p.split("=")[1]));
        }
        return params;
    }

    protected void handleResponse(HttpExchange httpExchange, String response, int responseCode) throws IOException {
        try (OutputStream outputStream = httpExchange.getResponseBody()) {
            httpExchange.sendResponseHeaders(responseCode, response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
        }
    }
}
