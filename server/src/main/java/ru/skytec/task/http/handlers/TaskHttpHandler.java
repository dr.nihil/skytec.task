package ru.skytec.task.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import ru.skytec.task.annotations.AutoConfig;
import ru.skytec.task.exceptions.RequestException;
import ru.skytec.task.services.impl.TaskService;

import java.util.Map;

public class TaskHttpHandler extends AbstractHttpHandler {
    private final TaskService taskService;

    @AutoConfig
    public TaskHttpHandler(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    protected String handleGetRequest(HttpExchange exchange) throws RequestException {
        throw new RequestException("Method " + exchange.getRequestMethod() + "isn't supported");
    }

    @Override
    protected String handlePutRequest(HttpExchange exchange) throws RequestException {
        throw new RequestException("Method " + exchange.getRequestMethod() + "isn't supported");
    }

    @Override
    protected String handlePostRequest(HttpExchange httpExchange) throws RequestException {
        long taskId = getIdFromPath(httpExchange.getRequestURI().getPath());
        Map<String, String> params = parseRequestParamsFromQuery(httpExchange.getRequestURI().getQuery());
        if (!params.containsKey("clanId")) {
            throw new RequestException("clanId parameter is missed");
        }
        long clanId = Long.parseLong(params.get("clanId"));
        if (taskService.completeTask(clanId, taskId)) {
            return "Task is completed";
        }
        throw new RequestException("Task isn't completed");
    }
}
