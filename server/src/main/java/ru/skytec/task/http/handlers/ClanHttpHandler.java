package ru.skytec.task.http.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import ru.skytec.task.exceptions.RequestException;
import ru.skytec.task.objects.Clan;
import ru.skytec.task.services.ClanService;
import ru.skytec.task.services.impl.UserAddGoldService;
import ru.skytec.task.annotations.*;

import java.io.*;
import java.util.Map;

public class ClanHttpHandler extends AbstractHttpHandler {
    private final ClanService clanService;
    private final UserAddGoldService userAddGoldService;

    @AutoConfig
    public ClanHttpHandler(ClanService clanService, UserAddGoldService userAddGoldService) {
        this.clanService = clanService;
        this.userAddGoldService = userAddGoldService;
    }

    @Override
    protected String handlePutRequest(HttpExchange httpExchange) throws RequestException {
        long clanId = getIdFromPath(httpExchange.getRequestURI().getPath());
        Map<String, String> params = parseRequestParamsFromQuery(httpExchange.getRequestURI().getQuery());
        if (!params.containsKey("userId")) {
            throw new RequestException("userId parameter is missed");
        }
        if (!params.containsKey("gold")) {
            throw new RequestException("gold parameter is missed");
        }
        long userId = Long.parseLong(params.get("userId"));
        int gold = Integer.parseInt(params.get("gold"));
        if (userAddGoldService.addGoldToClan(userId, clanId, gold)) {
            Clan clan = clanService.getClan(clanId);
            return clan.toString();
        }
        throw new RequestException("Couldn't add gold to clan");
    }

    @Override
    protected String handleGetRequest(HttpExchange httpExchange) throws RequestException {
        long clanId = getIdFromPath(httpExchange.getRequestURI().getPath());
        Clan clan = clanService.getClan(clanId);
        if (clan == null) {
            throw new RequestException("Clan not found");
        }
        return clan.toString();
    }

    @Override
    protected String handlePostRequest(HttpExchange httpExchange) throws RequestException {
        try (InputStreamReader isr = new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
             BufferedReader br = new BufferedReader(isr)) {
            Clan clan = clanService.addClan(new Gson().fromJson(br, Clan.class));
            if (clan == null) {
                throw new RequestException("Clan isn't created, please see logs");
            }
            return clan.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
