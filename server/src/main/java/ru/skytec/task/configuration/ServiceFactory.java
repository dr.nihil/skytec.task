package ru.skytec.task.configuration;

import java.util.Map;

public interface ServiceFactory {
    public <T> T getService(Class<T> objectClass, Map<String, Object> serviceCache);
}
