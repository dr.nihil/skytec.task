package ru.skytec.task.configuration.impl;

import org.reflections.Reflections;
import ru.skytec.task.annotations.AutoConfig;
import ru.skytec.task.annotations.Service;
import ru.skytec.task.configuration.ServiceFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ServiceFactoryImpl implements ServiceFactory {
    Reflections scanner;

    public ServiceFactoryImpl(String packageToScan) {
        scanner = new Reflections(packageToScan);
    }

    @Override
    public <T> T getService(Class<T> objectClass, Map<String, Object> serviceCache) {
        Class<? extends T> classImplementation = objectClass;
        T object;

        if (objectClass.isInterface()) {
            classImplementation = getClassImplementation(objectClass);
        }
        if (serviceCache.containsKey(classImplementation.getName())) {
            object = (T) serviceCache.get(classImplementation.getName());
        } else {
            object = createAndConfigureObject(classImplementation, serviceCache);
            if (classImplementation.isAnnotationPresent(Service.class)) {
                serviceCache.put(object.getClass().getName(), object);
            }
        }
        return object;
    }

    private <T> T createAndConfigureObject(Class<T> classImplementation, Map<String, Object> serviceCache) {
        try {
            for (Constructor<?> constructor : classImplementation.getDeclaredConstructors()) {
                if (constructor.isAnnotationPresent(AutoConfig.class)) {
                    return (T) constructor.newInstance(Arrays.stream(constructor.getParameters())
                            .map(param -> getService(param.getType(), serviceCache))
                            .collect(Collectors.toList()).toArray());
                }
                return (T) constructor.newInstance();
            }
            return classImplementation.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Object couldn't be created: " + classImplementation.getName(), e);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> Class<? extends T> getClassImplementation(Class<T> type) {
        Set<Class<? extends T>> classes = scanner.getSubTypesOf(type);
        if (!classes.iterator().hasNext()) {
            throw new RuntimeException("Couldn't find implementation for " + type.getName());
        }
        return classes.iterator().next();
    }
}
