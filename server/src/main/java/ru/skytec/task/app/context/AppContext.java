package ru.skytec.task.app.context;

import ru.skytec.task.configuration.ServiceFactory;
import ru.skytec.task.daos.ObserverDAO;
import ru.skytec.task.observer.impl.ObserverImpl;

import java.util.HashMap;
import java.util.Map;

public final class AppContext {
    private final Map<String, Object> serviceCache;
    private final ServiceFactory serviceFactory;

    public AppContext(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
        serviceCache = new HashMap<>();
        serviceCache.put(serviceFactory.getClass().getName(), serviceFactory);
    }

    public <T> T getObject(Class<T> objectClass) {
        return serviceFactory.getService(objectClass, serviceCache);
    }
}
