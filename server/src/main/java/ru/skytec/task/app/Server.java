package ru.skytec.task.app;

import com.sun.net.httpserver.HttpServer;
import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.app.context.AppContext;
import ru.skytec.task.app.init.AppInit;
import ru.skytec.task.http.handlers.ClanHttpHandler;
import ru.skytec.task.http.handlers.ObserverHttpHandler;
import ru.skytec.task.http.handlers.TaskHttpHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
public class Server {
    public static void main(String... args) throws IOException {
        AppContext appContext = AppInit.initialize();
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(100);
        HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 8080), 0);
        server.createContext("/clan", appContext.getObject(ClanHttpHandler.class));
        server.createContext("/events", appContext.getObject(ObserverHttpHandler.class));
        server.createContext("/tasks", appContext.getObject(TaskHttpHandler.class));
        server.setExecutor(threadPoolExecutor);
        log.info("Server is starting");
        server.start();
    }
}
