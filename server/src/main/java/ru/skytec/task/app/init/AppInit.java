package ru.skytec.task.app.init;

import ru.skytec.task.app.context.AppContext;
import ru.skytec.task.configuration.ServiceFactory;
import ru.skytec.task.configuration.impl.ServiceFactoryImpl;
import ru.skytec.task.dbinitializer.DbInit;

public class AppInit {
    public static AppContext initialize() {
        DbInit.initializeDB();
        ServiceFactory serviceFactory = new ServiceFactoryImpl("ru.skytec.task");
        return new AppContext(serviceFactory);
    }
}
