package ru.skytec.task.observer.impl;

import ru.skytec.task.observer.event.ChangeGoldEvent;
import ru.skytec.task.daos.ObserverDAO;
import ru.skytec.task.annotations.*;

import java.util.List;

@Service
public class ObserverImpl {
    private final ObserverDAO observerDAO;

    @AutoConfig
    public ObserverImpl(ObserverDAO observerDAO) {
        this.observerDAO = observerDAO;
    }

    public void eventUpdateGold(ChangeGoldEvent event) {
        observerDAO.saveChangeGoldEvent(event);
    }

    public List<ChangeGoldEvent> getEvents(int max, int offset) {
        return observerDAO.getEvents(max, offset);
    }
}
