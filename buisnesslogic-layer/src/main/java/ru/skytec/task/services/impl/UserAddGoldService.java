package ru.skytec.task.services.impl;

import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.annotations.*;
import ru.skytec.task.objects.Clan;
import ru.skytec.task.observer.event.ChangeGoldEvent;
import ru.skytec.task.observer.event.Event;
import ru.skytec.task.services.ClanService;
import ru.skytec.task.observer.impl.ObserverImpl;

@Service
@Slf4j
public class UserAddGoldService {
    private final ClanService clanService;
    private final ObserverImpl observer;

    @AutoConfig
    public UserAddGoldService(ClanService clanService, ObserverImpl observer) {
        this.clanService = clanService;
        this.observer = observer;
    }

    public boolean addGoldToClan(long userId, long clanId, int gold) {
        Clan clan = clanService.getClan(clanId);
        if (clan == null){
            log.error("Couldn't fing clan with id \"{}\"",clanId);
            return false;
        }
        int oldValue;
        int newValue;
        synchronized (clan) {
            oldValue = clan.getGold();
            newValue = oldValue + gold;
            clan.setGold(newValue);
        }
        boolean success = clanService.putGold(clan);
        if (success) {
            observer.eventUpdateGold(new ChangeGoldEvent.Builder(Event.EventType.ADD)
                    .timestamp(System.currentTimeMillis())
                    .sourceType("user")
                    .sourceId(userId)
                    .clanId(clanId)
                    .oldValue(oldValue)
                    .newValue(newValue)
                    .build());
        }
        return success;
    }
}
