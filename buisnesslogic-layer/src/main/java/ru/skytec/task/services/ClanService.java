package ru.skytec.task.services;

import ru.skytec.task.objects.Clan;

public interface ClanService {
    public Clan getClan(long id);

    public Clan addClan(Clan clan);

    public boolean putGold(Clan clan);
}
