package ru.skytec.task.services.impl;

import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.annotations.*;
import ru.skytec.task.objects.Clan;
import ru.skytec.task.services.ClanService;
import ru.skytec.task.daos.ClanDAO;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class ClanServiceImpl implements ClanService {

    private final ClanDAO clanDao;
    private final Map<Long, Clan> clansCache = new ConcurrentHashMap<>();

    @AutoConfig
    public ClanServiceImpl(ClanDAO clanDao) {
        this.clanDao = clanDao;
    }

    @Override
    public Clan getClan(long id) {
        Clan clan = clansCache.get(id);
        if (clan == null) {
            clan = clanDao.getClan(id);
            if (clan != null) {
                clan = clansCache.putIfAbsent(id, clan);
            }
        }
        return clan;
    }

    @Override
    public Clan addClan(Clan clan) {
        Clan clanNew = clanDao.addClan(clan);
        clansCache.put(clanNew.getId(), clanNew);
        log.info("{} is created", clanNew);
        return clanNew;
    }

    @Override
    public boolean putGold(Clan clan) {
        boolean success = false;
        synchronized (clan) {
            success = clanDao.updateGold(clan);
        }
        return success;
    }
}
