package ru.skytec.task.services.impl;

import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.annotations.*;
import ru.skytec.task.objects.Clan;
import ru.skytec.task.observer.event.ChangeGoldEvent;
import ru.skytec.task.observer.event.Event;
import ru.skytec.task.observer.impl.ObserverImpl;
import ru.skytec.task.services.ClanService;

@Service
@Slf4j
public class TaskService {
    private final ClanService clanService;
    private final ObserverImpl observer;
    private final int reward = 100;

    @AutoConfig
    public TaskService(ClanService clanService, ObserverImpl observer) {
        this.clanService = clanService;
        this.observer = observer;
    }

    public boolean completeTask(long clanId, long taskId) {
        Clan clan = clanService.getClan(clanId);
        if (clan == null){
            log.error("Couldn't fing clan with id \"{}\"",clanId);
            return false;
        }
        int oldValue;
        int newValue;
        synchronized (clan) {
            oldValue = clan.getGold();
            newValue = oldValue + reward;
            clan.setGold(newValue);
        }
        boolean success = clanService.putGold(clan);
        if (success) {
            observer.eventUpdateGold(new ChangeGoldEvent.Builder(Event.EventType.ADD)
                    .sourceType("task")
                    .sourceId(taskId)
                    .clanId(clanId)
                    .oldValue(oldValue)
                    .newValue(newValue)
                    .build());
        }
        return success;
    }
}
