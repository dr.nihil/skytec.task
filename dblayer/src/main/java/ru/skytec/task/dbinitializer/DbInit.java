package ru.skytec.task.dbinitializer;

import ru.skytec.task.datasources.HikariPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbInit {
    public static void initializeDB() {
        try (Connection con = HikariPool.getConnection()) {
            String createClanTable = "create table clan (" +
                    " id BIGINT NOT NULL primary key auto_increment," +
                    " name VARCHAR(50) NOT NULL unique," +
                    " gold BIGINT DEFAULT 0)";
            PreparedStatement stmt1 = con.prepareStatement(createClanTable);
            stmt1.executeUpdate();
            stmt1.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
