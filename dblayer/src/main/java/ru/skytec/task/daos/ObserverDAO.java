package ru.skytec.task.daos;

import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.annotations.*;
import ru.skytec.task.datasources.HikariPool;
import ru.skytec.task.observer.event.ChangeGoldEvent;
import ru.skytec.task.observer.event.Event;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ObserverDAO {
    public ObserverDAO() {
        try (Connection con = HikariPool.getConnection()) {
            String createEventTable = "create table event" +
                    " (id BIGINT NOT NULL primary key auto_increment," +
                    " timestamp BIGINT NOT NULL," +
                    " event varchar(10) NOT NULL," +
                    " source varchar(50) NOT NULL," +
                    " source_id BIGINT NOT NULL," +
                    " clan_id BIGINT NOT NULL," +
                    " old_value INT NOT NULL," +
                    " new_value INT NOT NULL)";
            PreparedStatement stmt2 = con.prepareStatement(createEventTable);
            stmt2.executeUpdate();
            stmt2.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveChangeGoldEvent(ChangeGoldEvent event) {
        try (Connection con = HikariPool.getConnection()) {
            String sql = "insert into event (timestamp,event,source,source_id,clan_id,old_value,new_value) values (?,?,?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, event.getTimestamp());
            stmt.setString(2, event.getEventType().name());
            stmt.setString(3, event.getSourceType());
            stmt.setLong(4, event.getSourceId());
            stmt.setLong(5, event.getClanId());
            stmt.setLong(6, event.getOldValue());
            stmt.setLong(7, event.getNewValue());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            log.error("Can't save event (event type - {}, source type - {}, source id - {}, clan id - {},old value - {}, new value - {})",
                    event.getEventType().name(), event.getSourceType(), event.getSourceId(), event.getClanId(), event.getOldValue(), event.getNewValue());
        }
    }

    public List<ChangeGoldEvent> getEvents(int max, int offset) {
        List<ChangeGoldEvent> events = new ArrayList<>();
        try (Connection con = HikariPool.getConnection()) {
            String sql = "select * from event order by timestamp limit ? offset ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, max);
            stmt.setInt(2, offset);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                events.add(new ChangeGoldEvent.Builder(Event.EventType.valueOf(rs.getString("event")))
                        .timestamp(rs.getLong("timestamp"))
                        .sourceType(rs.getString("source"))
                        .sourceId(rs.getLong("source_id"))
                        .clanId(rs.getLong("clan_id"))
                        .oldValue(rs.getInt("old_value"))
                        .newValue(rs.getInt("new_value"))
                        .build());
            }
        } catch (SQLException e) {
            log.error("Can't get events", e);
        }
        return events;
    }
}
