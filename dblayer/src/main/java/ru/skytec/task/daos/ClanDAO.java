package ru.skytec.task.daos;

import lombok.extern.slf4j.Slf4j;
import ru.skytec.task.datasources.HikariPool;
import ru.skytec.task.objects.Clan;
import ru.skytec.task.annotations.*;

import java.sql.*;

@Slf4j
@Service
public class ClanDAO {

    public ClanDAO() {
    }

    public Clan getClan(long id) {
        Clan clan = null;
        try (Connection con = HikariPool.getConnection()) {
            String sql = "select * from clan where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                clan = new Clan(rs.getLong("id"), rs.getString("name"));
                clan.setGold(rs.getInt("gold"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            log.error("Can't get clan from DB", e);
        }
        return clan;
    }

    public Clan addClan(Clan clan) {
        try (Connection con = HikariPool.getConnection()) {
            String sql = "insert into clan (name) values (?)";
            PreparedStatement stmt = con.prepareStatement(sql, new String[]{"id"});
            stmt.setString(1, clan.getName());
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                clan = new Clan(rs.getLong(1), clan.getName());
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            log.error("Can't create clan from DB", e);
            return null;
        }
        return clan;
    }

    public boolean updateGold(Clan clan) {
        try (Connection con = HikariPool.getConnection()) {
            String sql = "update clan set gold = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, clan.getGold());
        } catch (SQLException e) {
            log.error("Can't add gold to clan {} from DB", clan.getName(), e);
            return false;
        }
        return true;
    }
}
